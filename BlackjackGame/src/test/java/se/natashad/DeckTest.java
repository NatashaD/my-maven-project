package se.natashad;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;



public class DeckTest {
	Deck deck = new Deck ();

	@Test
	public void test_no_duplicate_cards() {
			
		while (deck.cards.size() > 0) {
			Card card = deck.draw();

			for (int i = 0; i < deck.cards.size(); i++) {
				assertNotSame(card, deck.cards.get(i));
			}
		}

	}
		
	@Test
	public void test_no_duplicate_cards_with_shuffle() {
		deck.shuffle();
		
		while (deck.cards.size() > 0) {
			Card card = deck.draw();

			for (int i = 0; i < deck.cards.size(); i++) {
				assertNotSame(card, deck.cards.get(i));
			}
		}

	}
				
	
	@Test
	public void deck_size_should_be_fiftytwo_before_and_after_shuffle() {
			
			
			System.out.println("Deck size before shuffle : "+ deck.cards.size());
			assertTrue(deck.cards.size() == 52);

			deck.shuffle();
			assertTrue(deck.cards.size() == 52);
			System.out.println("Deck size after shuffle : "+ deck.cards.size());
		
	}
	
	@Test
	public void test_Draw() {
		deck.draw();
		assertNotEquals(52, deck.cards.size());
		System.out.println("Deck size after draw : " + deck.cards.size());
			
	}
}

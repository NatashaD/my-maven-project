package se.natashad;

public class Card {

	private Suit suit;
	private int value;
	
	public Card(int value, Suit suit){
		this.suit = suit;
		this.value = value;
		}
	
	public Suit getSuit() {
		return suit;}
	
	public int getValue(){
		return value;}

	@Override
	public String toString(){
		return value + " of " + suit;
	}
	
}
